window.YTD.ad_engagements.part0 = [ {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1242885285454675968",
              "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-03-27 20:18:08"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-03-27 20:18:19",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-03-27 20:18:30",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-03-27 20:18:12",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-03-27 20:18:18",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-03-27 20:18:16",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-03-27 20:18:16",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-03-27 20:18:11",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-03-27 20:18:10",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-03-27 20:18:14",
            "engagementType" : "VideoContentPlayback50"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1233321174257545216",
              "tweetText" : "kDrive: Die neue cloudbasierte Collaboration-Lösung von Infomaniak 🇨🇭\n\nJetzt 30 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/Lv1NdYJA99 https://t.co/4vFjNSeYVr",
              "urls" : [ "https://t.co/Lv1NdYJA99" ],
              "mediaUrls" : [ "https://t.co/4vFjNSeYVr" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infomaniak",
              "screenName" : "@infomaniak"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@awscloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Dropbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@googledrive"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#privacy"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "hostpoint"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "privacy"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "cloud computing"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "purchase"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "account signup"
            }, {
              "targetingType" : "Tailored audiences (web)"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-03-28 11:44:25"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-03-28 11:44:30",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-03-28 11:44:32",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-03-28 11:44:30",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-03-28 11:44:28",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-03-28 11:44:34",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-03-28 11:44:37",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-03-28 11:44:35",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-03-28 13:29:07",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-03-28 11:44:37",
            "engagementType" : "VideoContentPlayback95"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514899310542848",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 04:18:36"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 04:18:38",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1245267569755463681",
              "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
              "urls" : [ "https://t.co/I4grLgGEqj" ],
              "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infomaniak",
              "screenName" : "@infomaniak"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@awscloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@googledrive"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Datenschutz"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "purchase"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "account signup"
            }, {
              "targetingType" : "Tailored audiences (web)"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-09 03:58:27"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 04:18:38",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 03:58:38",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-09 03:58:32",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 03:58:34",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 03:58:31",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 03:58:36",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 03:58:38",
            "engagementType" : "VideoContentPlayback95"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514947989671937",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 03:17:02"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 03:17:08",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 03:17:12",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 03:17:12",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-09 03:17:07",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-09 03:17:09",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-09 03:17:21",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 03:17:07",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-09 03:17:05",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 03:17:05",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-09 03:17:09",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-09 03:17:02"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 03:26:29",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-09 03:26:32",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-09 03:26:34",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-09 03:26:28",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-09 03:26:27",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-09 03:26:35",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-09 03:26:25",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 03:35:49",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 03:26:29",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-09 03:26:32",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 03:26:30",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 03:26:27",
            "engagementType" : "VideoContentPlayback25"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 07:45:40"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 07:45:46",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 07:45:53",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 07:45:42",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-09 07:45:49",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 07:45:54",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 07:45:43",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514844566597635",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-09 11:19:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 11:20:47",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-09 11:19:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-09 11:20:00",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-09 11:20:05",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-09 11:20:03",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-09 11:20:01",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-09 11:20:08",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-09 11:20:07",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-09 11:20:01",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-09 11:20:02",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-09 11:20:02",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-09 11:20:31",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-09 11:19:59",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-09 11:20:05",
            "engagementType" : "VideoContent6secView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1093107212556345344",
              "tweetText" : "Private virtual Platform as a Service for developers.\nLots of features available from the box.\nTry for free!\n https://t.co/6cuN6zzRhI \n#d2cio #docker #4devs https://t.co/37UxQaNQxg",
              "urls" : [ "https://t.co/6cuN6zzRhI" ],
              "mediaUrls" : [ "https://t.co/37UxQaNQxg" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "D2C.io",
              "screenName" : "@d2cio"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@github"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Azure"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ThePSF"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nodejs"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@MongoDB"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@GCPcloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ubuntu"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Linux"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@debian"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@codinghorror"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@awscloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Docker"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "devops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "linux"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "golang"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "javascript"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "github"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "python"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#golang"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "13 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-10 06:39:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:40:25",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:40:31",
            "engagementType" : "VideoSession"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240578761495425040",
              "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-10 06:22:04"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:22:10",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-10 06:22:11",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-10 06:22:14",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-10 06:22:14",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-10 06:22:09",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-10 06:22:17",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-10 06:23:16",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-10 06:22:10",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-10 06:22:11",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-10 06:22:12",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-10 06:22:08",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:22:17",
            "engagementType" : "VideoContentPlaybackComplete"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1181307044550381569",
              "tweetText" : "Correlate logs with metrics and traces, easily investigate trends with the Log Patterns view, and now, re-index archived logs to provide historical context. Learn more here: https://t.co/MnoLgq4YuK",
              "urls" : [ "https://t.co/MnoLgq4YuK" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Datadog, Inc.",
              "screenName" : "@datadoghq"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@rapid7"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "DevOps"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#DevOps"
            }, {
              "targetingType" : "Tailored audiences (lists)",
              "targetingValue" : "AWS Customers v2"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-10 06:39:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:40:25",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-10 06:40:17",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-10 06:40:18",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-10 06:40:15",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:40:16",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-10 06:40:18",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1247479734000406528",
              "tweetText" : "Welche Chancen haben Menschen ohne Zugang zu funktionierenden medizinischen und sanitären Einrichtungen im Kampf gegen #COVIDー19 ?\n\nGemeinsam müssen wir jenen helfen, die gefährdet sind.\n👉 https://t.co/SMynVT5eLg https://t.co/2L5gWypIVg",
              "urls" : [ "https://t.co/SMynVT5eLg" ],
              "mediaUrls" : [ "https://t.co/2L5gWypIVg" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "IKRK",
              "screenName" : "@IKRK"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ICRC"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@RotesKreuz_CH"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-10 06:22:04"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 06:22:49",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-10 06:22:43",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-10 06:22:37",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 06:22:55",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-10 06:22:43",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-10 06:25:18",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-10 06:23:00",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-10 06:22:41",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-10 06:22:41",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-10 06:23:01",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-10 06:22:39",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-10 06:22:39",
            "engagementType" : "VideoContent1secView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-10 12:08:58"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-10 14:38:59",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-10 14:39:00",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1245267569755463681",
              "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
              "urls" : [ "https://t.co/I4grLgGEqj" ],
              "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infomaniak",
              "screenName" : "@infomaniak"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Dropbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@googledrive"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Datenschutz"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "purchase"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "account signup"
            }, {
              "targetingType" : "Tailored audiences (web)"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-24 07:29:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-24 07:30:00",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-24 07:30:02",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-24 07:29:58",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-24 07:29:56",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-24 07:32:23",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-24 07:29:58",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-24 07:30:02",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-24 07:29:55",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-24 07:30:02",
            "engagementType" : "VideoContent6secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-24 07:29:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-24 07:29:51",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-24 07:29:34",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-24 07:29:56",
            "engagementType" : "VideoSession"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242885285454675968",
              "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 03:17:12"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 03:27:23",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 04:06:18",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 04:04:09",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-01 03:17:19",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 03:17:21",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 03:17:13",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-01 04:04:10",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-01 03:17:18",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 03:17:15",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610359283719",
              "tweetText" : "Keine Lust auf sprunghafte Berater? Nimm deine Säule 3a selbst in die Hand. #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 03:27:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 03:28:46",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-01 03:29:14",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 03:28:47",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-01 03:28:45",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-01 03:28:48",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 03:28:47",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-01 03:28:44",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 03:28:46",
            "engagementType" : "VideoContentPlayback25"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 08:34:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 08:39:39",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-01 08:39:36",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-01 08:39:35",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-01 08:39:37",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-01 08:39:35",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 08:39:32",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 08:39:30",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 08:39:55",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 08:39:35",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-01 08:39:32",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-01 08:39:32",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-01 08:39:39",
            "engagementType" : "VideoContentPlaybackComplete"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242885285454675968",
              "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 08:34:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 08:38:32",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-01 08:38:30",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 08:38:32",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-01 08:38:34",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-01 08:39:32",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 08:38:29",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-01 08:38:34",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 08:38:31",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 08:38:34",
            "engagementType" : "VideoContentViewV2"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514899310542848",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-01 08:34:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 08:34:57",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1245267569755463681",
              "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
              "urls" : [ "https://t.co/I4grLgGEqj" ],
              "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infomaniak",
              "screenName" : "@infomaniak"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@awscloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Dropbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@googledrive"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Datenschutz"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "purchase"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "account signup"
            }, {
              "targetingType" : "Tailored audiences (web)"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 08:47:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 08:48:00",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-01 08:47:59",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 08:48:00",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-01 08:48:00",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-01 08:47:59",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-01 08:48:21",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 08:48:04",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-01 08:48:03",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-01 08:47:57",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 08:48:04",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-01 08:48:00",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 08:48:04",
            "engagementType" : "VideoContentPlaybackComplete"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1237645336203624449",
              "tweetText" : "Unsere Hypothekarexperten verkuppeln Sie mit Ihrer Traum-Hypothek. #valuu",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Valuu",
              "screenName" : "@valuuapp"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 08:34:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 08:39:49",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-01 08:39:49",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 08:39:52",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-01 08:39:53",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-01 08:39:51",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-01 08:39:51",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-01 08:41:21",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 08:39:48",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 08:39:52",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-01 08:39:47",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 08:39:51",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-01 08:39:49",
            "engagementType" : "VideoContentMrcView"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-01 18:00:03"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 18:00:05",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514899310542848",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-01 15:48:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 15:49:48",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1237645839138525184",
              "tweetText" : "In Immobilien investieren? Valuu verkuppelt Sie mit der besten Hypothek dazu. #valuu",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Valuu",
              "screenName" : "@valuuapp"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 15:48:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 15:50:21",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 15:50:14",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 15:50:13",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242885285454675968",
              "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 15:48:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 15:50:16",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 15:50:29",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 15:50:16",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-01 15:50:18",
            "engagementType" : "VideoContentPlayback25"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240578761495425040",
              "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 15:48:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 15:49:37",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-01 15:49:06",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-01 15:49:08",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-01 15:49:39",
            "engagementType" : "VideoContentPlayFromTapV2"
          }, {
            "engagementTime" : "2020-04-01 15:49:03",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 15:49:08",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-01 15:49:52",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 15:48:59",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 15:49:01",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-01 15:49:01",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 15:49:37",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-01 15:49:05",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-01 15:49:35",
            "engagementType" : "VideoContent1secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-01 17:57:46"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-01 17:58:14",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-01 17:58:01",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-01 17:57:49",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-01 17:57:51",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-01 17:58:02",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-01 17:58:00",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-01 17:57:53",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-01 17:57:51",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-01 17:58:00",
            "engagementType" : "VideoContentPlayback75"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-02 00:49:48"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-02 00:52:14",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-02 00:50:23",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-02 00:50:20",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-02 00:50:34",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-02 00:50:26",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-02 00:50:23",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-02 00:50:20",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-02 00:50:26",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-02 00:50:21",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-02 00:50:33",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-02 00:50:22",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-02 00:50:30",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-02 00:50:23",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1237645839138525184",
              "tweetText" : "In Immobilien investieren? Valuu verkuppelt Sie mit der besten Hypothek dazu. #valuu",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Valuu",
              "screenName" : "@valuuapp"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-02 00:32:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-02 00:35:33",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-02 00:35:34",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-02 00:35:31",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-02 00:35:31",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-02 00:38:56",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-02 00:35:30",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-02 00:35:34",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-02 00:35:29",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-02 00:35:32",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-02 00:38:52",
            "engagementType" : "VideoContent6secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1222927367745736709",
              "tweetText" : "Start collecting enhanced AWS Lambda metrics in real-time with Datadog and begin tracking the performance of your serverless functions seconds after they've been invoked. Start a free trial today:",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Datadog, Inc.",
              "screenName" : "@datadoghq"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer programming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Enterprise software"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Open source"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "DevOps"
            }, {
              "targetingType" : "Tailored audiences (lists)",
              "targetingValue" : "AWS Customers v2"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-02 00:32:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-02 00:39:42",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-02 00:39:44",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-02 00:39:59",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-02 00:39:44",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-02 00:39:44",
            "engagementType" : "VideoContent1secView"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240631610426494978",
              "tweetText" : "Mit frankly kannst du deine Säule 3a schnell und ohne Umwege selbst verwalten.#duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-02 00:32:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-02 00:35:33",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-02 00:35:20",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244630934231580673",
              "tweetText" : "@kariem_hussein hält euch mit Videos über mentale Stärke, Ausdauer und Wohlbefinden fit. Alles dazu findet ihr in unserem Blog ➡️ https://t.co/oBrSXE6Zai \n#soschützenwiruns #bleibtzuhause #stayathome #sport #running #motivation #prävention #laufsport #laufen #groupemutuel https://t.co/FSpLLNiXoR",
              "urls" : [ "https://t.co/oBrSXE6Zai" ],
              "mediaUrls" : [ "https://t.co/FSpLLNiXoR" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Groupe Mutuel",
              "screenName" : "@Groupe_Mutuel"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-02 00:42:13"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-02 00:42:47",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242874852836589575",
              "tweetText" : "Für eure Service-Anliegen sind die Swisscom Shops an den wichtigsten Standorten von Mo–Sa von 10 bis 15 Uhr geöffnet.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-02 00:32:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-02 00:32:22",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-02 00:32:31",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-02 00:32:35",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-02 00:34:54",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-02 00:32:37",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-02 00:32:39",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-02 00:32:33",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-02 00:32:37",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-02 00:32:21",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-02 00:32:33",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-02 00:32:32",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-02 00:32:20",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-02 00:32:33",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-02 00:32:39",
            "engagementType" : "VideoContentPlaybackComplete"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1231881545398525952",
              "tweetText" : "Die digitale Telefonie bietet zahlreiche Vorteile - gerade auch für #KMU:\n#VoIP Lösungen sind flexibel, kostengünstig und einfach zu bedienen.  \n\nLesen Sie hier anhand eines Kundenbeispiels, welche Vorteile VoIP in der Praxis bringen:\n▶ https://t.co/TdM3xMUiWr https://t.co/PPOX0wwnFR",
              "urls" : [ "https://t.co/TdM3xMUiWr" ],
              "mediaUrls" : [ "https://t.co/PPOX0wwnFR" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infortix",
              "screenName" : "@infortix_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Enterprise software"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "B2B"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "KMU"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 01:30:37"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 01:30:38",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1245471249368641536",
              "tweetText" : "Pas envie de conseils aléatoires ? Prends ton pilier 3a en main. #cesttoiquidécides",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "promotedTrendInfo" : {
              "trendId" : "71985",
              "name" : "#franklyzkb20200403",
              "description" : ""
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 01:04:59"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 01:05:07",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-03 01:05:04",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 01:05:09",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-03 01:05:05",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-03 01:05:12",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-03 01:05:04",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 01:05:02",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 01:05:11",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-03 01:05:06",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-03 01:05:09",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-03 01:05:06",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240578761495425040",
              "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 02:11:14"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 02:19:22",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 02:19:24",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-03 02:19:23",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 02:19:21",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242874852836589575",
              "tweetText" : "Für eure Service-Anliegen sind die Swisscom Shops an den wichtigsten Standorten von Mo–Sa von 10 bis 15 Uhr geöffnet.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 02:11:14"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 02:19:36",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 02:19:34",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1237645839138525184",
              "tweetText" : "In Immobilien investieren? Valuu verkuppelt Sie mit der besten Hypothek dazu. #valuu",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "promotedTrendInfo" : {
              "trendId" : "72405",
              "name" : "#valuu",
              "description" : ""
            },
            "advertiserInfo" : {
              "advertiserName" : "Valuu",
              "screenName" : "@valuuapp"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 22:28:51"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 22:35:58",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 22:36:50",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-03 22:35:58",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242879802635423744",
              "tweetText" : "Zur Ablenkung für unsere Swisscom TV-Kunden gibt es im April die besten Filme gratis auf Teleclub Movie.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 22:28:51"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 22:36:48",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-03 22:36:47",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-03 22:36:48",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-03 22:36:44",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-03 22:36:46",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 22:36:46",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 22:36:44",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 22:37:06",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-03 22:36:49",
            "engagementType" : "VideoContentPlayback50"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1240578761495425040",
              "tweetText" : "Bei diesen Zinsen lohnt sich Wertschriftensparen noch mehr.  #duhastesinderhand",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "frankly.",
              "screenName" : "@frankly_zkb"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 21:16:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:29:08",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 21:29:08",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 21:29:06",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 21:29:38",
            "engagementType" : "VideoSession"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242885285454675968",
              "tweetText" : "Gestrandete Kunden, die sich unfreiwillig im Ausland befinden, erhalten eine Roaming Gutschrift bis zu CHF 200.-",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 21:16:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:16:24",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-03 21:16:29",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 21:16:30",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-03 21:16:34",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-03 21:16:31",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-03 21:29:09",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-03 21:27:03",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-03 21:16:29",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 21:16:31",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-03 21:16:36",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-03 21:16:32",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-03 21:16:28",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 21:16:34",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-03 21:16:37",
            "engagementType" : "VideoContentPlaybackComplete"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1245267569755463681",
              "tweetText" : "Pläne mit 2 TB, 3 TB und 6 TB und mehr! 🚀\nIhr heiss ersehnter Speicherplatz für CHF 5.95/Monat \n\nJetzt 90 Tage kostenlos und unverbindlich testen!\n➡️ https://t.co/I4grLgGEqj https://t.co/gnIylA6seB",
              "urls" : [ "https://t.co/I4grLgGEqj" ],
              "mediaUrls" : [ "https://t.co/gnIylA6seB" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infomaniak",
              "screenName" : "@infomaniak"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@awscloud"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Dropbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@googledrive"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Datenschutz"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "purchase"
            }, {
              "targetingType" : "Tailored audiences (web)",
              "targetingValue" : "account signup"
            }, {
              "targetingType" : "Tailored audiences (web)"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 21:16:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:30:01",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 21:29:59",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 21:30:27",
            "engagementType" : "VideoSession"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1104028125376270342",
              "tweetText" : "Plötzlich PHP 7 - Wir retten Ihre Webseite! Migrieren Sie jetzt, Ihre Webseite auf WordPress. https://t.co/7mXXC5OTPU https://t.co/ZHmKAbQTGs",
              "urls" : [ "https://t.co/7mXXC5OTPU" ],
              "mediaUrls" : [ "https://t.co/ZHmKAbQTGs" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "OVA oliver von arx + partner",
              "screenName" : "@Social_Media_CH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Business news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Government"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Green solutions"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Leadership"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 21:16:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:31:26",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 21:31:32",
            "engagementType" : "VideoSession"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1181307044550381569",
              "tweetText" : "Correlate logs with metrics and traces, easily investigate trends with the Log Patterns view, and now, re-index archived logs to provide historical context. Learn more here: https://t.co/MnoLgq4YuK",
              "urls" : [ "https://t.co/MnoLgq4YuK" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Datadog, Inc.",
              "screenName" : "@datadoghq"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer programming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Enterprise software"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Open source"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "DevOps"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#debugging"
            }, {
              "targetingType" : "Tailored audiences (lists)",
              "targetingValue" : "AWS Customers v2"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-03 21:33:23"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:33:34",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-03 21:33:43",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-03 21:33:35",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-03 21:33:33",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 21:33:37",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-03 21:33:32",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 21:33:35",
            "engagementType" : "VideoContentViewThreshold"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514970320207872",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-03 21:35:17"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:35:27",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-03 21:35:21",
            "engagementType" : "ChargeableImpression"
          }, {
            "engagementTime" : "2020-04-03 21:35:22",
            "engagementType" : "VideoContentPlaybackStart"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1237645336203624449",
              "tweetText" : "Unsere Hypothekarexperten verkuppeln Sie mit Ihrer Traum-Hypothek. #valuu",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Valuu",
              "screenName" : "@valuuapp"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 21:16:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:29:38",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 21:29:39",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 21:29:40",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-03 21:29:39",
            "engagementType" : "VideoContentPlayback25"
          }, {
            "engagementTime" : "2020-04-03 21:30:02",
            "engagementType" : "VideoSession"
          }, {
            "engagementTime" : "2020-04-03 21:29:40",
            "engagementType" : "VideoContentPlayback50"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1232967401236574214",
              "tweetText" : "⚠ Eines der grössten IT-Risiken: mangelhaft unterhaltene #Firewall! \n🤝Daher bieten wir #ManagedFirewall Lösungen an. Hardware &amp; Software werden von uns zur Verfügung gestellt &amp; laufend betreut, damit Sie sich auf Ihr Geschäft konzentrieren könnnen.\n\nℹ https://t.co/pNP3C4P4ZF https://t.co/GeUff0RPyW",
              "urls" : [ "https://t.co/pNP3C4P4ZF" ],
              "mediaUrls" : [ "https://t.co/GeUff0RPyW" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infortix",
              "screenName" : "@infortix_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Enterprise software"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "B2B"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "KMU"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 21:26:57"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 21:28:33",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1242874852836589575",
              "tweetText" : "Für eure Service-Anliegen sind die Swisscom Shops an den wichtigsten Standorten von Mo–Sa von 10 bis 15 Uhr geöffnet.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swisscom",
              "screenName" : "@Swisscom_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-04-03 14:40:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-03 14:40:28",
            "engagementType" : "VideoContentPlayback50"
          }, {
            "engagementTime" : "2020-04-03 14:40:30",
            "engagementType" : "VideoContentPlayback75"
          }, {
            "engagementTime" : "2020-04-03 14:40:27",
            "engagementType" : "VideoContentViewThreshold"
          }, {
            "engagementTime" : "2020-04-03 14:40:27",
            "engagementType" : "VideoContentViewV2"
          }, {
            "engagementTime" : "2020-04-03 14:40:33",
            "engagementType" : "VideoContentPlaybackComplete"
          }, {
            "engagementTime" : "2020-04-03 14:40:24",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-03 14:40:30",
            "engagementType" : "VideoContent6secView"
          }, {
            "engagementTime" : "2020-04-03 14:40:26",
            "engagementType" : "VideoContentMrcView"
          }, {
            "engagementTime" : "2020-04-03 14:40:25",
            "engagementType" : "VideoContent1secView"
          }, {
            "engagementTime" : "2020-04-03 14:40:32",
            "engagementType" : "VideoContentPlayback95"
          }, {
            "engagementTime" : "2020-04-03 14:40:26",
            "engagementType" : "VideoContentPlayback25"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514947989671937",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-04 05:33:01"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-04 05:33:05",
            "engagementType" : "VideoContentPlaybackStart"
          }, {
            "engagementTime" : "2020-04-04 05:33:04",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514899310542848",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-06 19:59:09"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-06 19:59:11",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "ProfileTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1244514899310542848",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-08 08:50:25"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-08 08:50:26",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1244514899310542848",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-08 06:23:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-08 06:23:44",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "ProfileTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1244514844566597635",
              "tweetText" : "Schauen Sie brandaktuelle Nachrichten aus der Schweiz, der Welt, der Gesellschaft, der Politik und dem Sport auf dem Smartphone oder dem PC live.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BLICK",
              "screenName" : "@Blickch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-04-08 06:16:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-04-08 06:16:17",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
} ]